# Scratch3-Move-and-Jump
This is a very basic tutorial on how to move a sprite and jump smoothly.

Related project - [Scratch-3 Movement](https://github.com/zleap/scratch3_movement). 

When playing games,  we sometimes need to jump over other sprites in the game to avoid colliding with them. 

Before we start remember that:

x|  left <> right|
|--|--|
y  | up < > down |


One basic method is to just move our sprite up from the current position.
![basic jump](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/smooth-glide1.png)

With this we start by moving our cat sprite in to the bottom left of the stage area.  When we press space the cat jumps up and then moves back down to where it started. 

We need to improve this a little. 

![basic jump 2](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/smoothglide2.png)

By using the xposition block :

![x-pos-block](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/xposition.png)

We can keep the cat relative to the x position of the stage area.    So regardless of where on the x axis we the cat will jump up then land where it started. 

Now add some more blocks to allow the cat to walk left and right, then and also jump.  

![jumpandmove](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/smooth-glide3.png) 

However moving right is fine,  moving to the left we end up with cat upside down.  This can be fixed by editing the **direction** attributes of the cat. 

![enter image description here](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/sprite-direction.png)

This should allow you to move left / right then jump and land back down where you started. 

What we can't at present is jump and move left or right at the same time.   

--

Created by Paul Sutton [Website](https://personaljournal.ca/paulsutton/) on January 6th 2019

![cc-logo](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/88x31.png)

![Download sb3 project file](https://github.com/zleap/Scratch3-Move-and-Jump/blob/master/basic-moveandjump.sb3). 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU4ODI0ODEyNl19
-->
